﻿namespace UEGP3_SoSe_RefReq.Code.Runtime.Turnbased
{
	public enum GridNodeType
	{
		Ground,
		Wall,
		Water,
		Cover,
	}
}