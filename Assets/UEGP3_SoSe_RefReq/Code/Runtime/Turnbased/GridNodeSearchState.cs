﻿namespace UEGP3_SoSe_RefReq.Code.Runtime.Turnbased
{
	public enum GridNodeSearchState
	{
		None,
		Queue,
		Processed,
		PartOfPath,
	}
}