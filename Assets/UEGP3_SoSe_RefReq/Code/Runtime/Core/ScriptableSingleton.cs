﻿using UnityEditor;
using UnityEngine;

namespace UEPG3_SoSe2020_RefReq.Core
{
	public abstract class ScriptableSingleton<T> : ScriptableObject where T : ScriptableSingleton<T>
	{
		private static T _instance;
		public static T Instance
		{
			get
			{
				if (_instance == null)
				{
					_instance = AssetDatabase.LoadAssetAtPath<T>($"Assets/UEGP3_SoSe_RefReq/ScriptableObjects/{typeof(T).Name}.asset");
				}
				
				return _instance;
			}
		}
	}
}