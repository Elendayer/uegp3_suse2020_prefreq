﻿using UnityEditor;
using UnityEngine;
using Grid = UEGP3_SoSe_RefReq.Code.Runtime.Turnbased.Grid;

namespace UEGP3_SoSe_RefReq.Code.Editor.Turnbased.Settings
{
	[CustomEditor(typeof(Grid), true)]
	public class GridEditor : UnityEditor.Editor
	{
		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();

			if (GUILayout.Button("Save"))
			{
				(target as Grid)?.SaveGrid();
			}
			
			if (GUILayout.Button("Load"))
			{
				(target as Grid)?.LoadGrid();
			}
		}
	}
}