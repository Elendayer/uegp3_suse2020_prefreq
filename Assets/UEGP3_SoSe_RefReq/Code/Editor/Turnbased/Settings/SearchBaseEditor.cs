﻿using UEGP3_SoSe_RefReq.Code.Runtime.Turnbased;
using UnityEditor;
using UnityEngine;

namespace UEGP3_SoSe_RefReq.Code.Editor.Turnbased.Settings
{
	[CustomEditor(typeof(SearchBase), true)]
	public class SearchBaseEditor : UnityEditor.Editor
	{
		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();

			if (GUILayout.Button("Perform Search"))
			{
				(target as SearchBase)?.Search();
			}
		}
	}
}